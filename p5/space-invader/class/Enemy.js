var img_basic;
var img_great_head;

function preload(){
	img_basic = loadImage('assets/basic_alien.png');
	img_great_head = loadImage('assets/great_head_alien.png');
}


function Enemy(x,y){
	this.x = x;
	this.y = y;
	this.speed = 2;

	this.getAppearance = function(){
		let myAppeareanceFunction;
		if(Math.floor(Math.random()*2) == 0){
			this.width = 50;
			this.height = 30;
			myAppeareanceFunction = function(){
			image(img_basic,this.x,this.y,this.width,this.height);
			}
		}
		else{
			this.width = 30;
			this.height = 50;
			myAppeareanceFunction = function(){
			image(img_great_head,this.x,this.y,this.width,this.height);
			}
		}

		this.show = myAppeareanceFunction;
	}

	this.move = function(dir){
		dir ? this.x -= this.speed : this.x += this.speed;
	}
}