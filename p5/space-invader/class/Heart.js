function Heart(x,y){
	this.x = x;
	this.y = y;
	this.width = 5;
	this.height = 10;


	this.show = function(){
		fill(255,0,0);
		quad(this.x,this.y,this.x+this.width,this.y+this.height/2,this.x,this.y+this.height,this.x-this.width,this.y + this.height/2);
	}
}