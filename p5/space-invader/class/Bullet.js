function Bullet(x,y,speed = 10){
	this.x = x;
	this.y = y;
	this.speed = speed;
	this.width = 10;
	this.height = 20;

	this.show = function(){
		fill(255);
		ellipse(this.x,this.y,this.width,this.height);
	}

	this.move = function(){
		this.y -= this.speed;
	}
}