var color_counter = 0;
var change_color = false;

function Player(){
	this.x = width/2;
	this.y = height - 50;
	this.width = 20;
	this.height = 30;
	this.bottom = this.y + this.height;
	this.speed = 10;
	this.left = false;
	this.right = false;
	this.ammunition = 1;
	this.bulletSpeed = 10;
	this.life = 2;
	this.change_color = false;


	this.show = function(){

		change_color ? fill(168,168,0) : fill(168,44,37);
		triangle(this.x, this.y, this.x - this.width,this.bottom,this.x + this.width, this.bottom);
	}

	this.move = function(){
		if(this.right && this.x < width-this.width) this.x += this.speed;
		if(this.left && this.x > this.width) this.x -= this.speed;
	}

	this.shine = function(){
		if(color_counter <4){
			change_color = !change_color;
			color_counter++;
		}
		else{
			clearInterval(interval);
			color_counter = 0;
		}

	}

	this.loseLife = function(){
		if(this.life > 0) loseHeart();
		else return true;
	}


}
