var player;
var pause = false;
var changing_level = false;

var enemy_array = [];
var enemy_lines = 1;
var enemies_per_line = 1;
var rows_before_landing = 20;
var enemy_space_y = 0;
var toggle_enemies_direction = false;

var bullet_array = [];
var heart_array = [];
var bonus_array = [];

var x_heart = 20;
var y_heart = 20;

var score = 0;
var scoreWave = 1;

var interval = null;



////////////////////// SETUP //////////////////////

function setup() {
  createCanvas(1500,800);
  player = new Player();
  createLevel();
  createLife();
}

////////////////////// ENEMIES //////////////////////

function createEnemies(){
	let positioning_x = 0;
	let positioning_y = 0;
	enemy_space_y = height/rows_before_landing;

	for (let i =  0; i < enemy_lines; i++) {
		positioning_y = enemy_space_y + i * enemy_space_y;
		for (let j =  0; j < enemies_per_line; j++) {
			positioning_x = width/(enemies_per_line*2) + (width/enemies_per_line) * j;
			let enemy = new Enemy(positioning_x,positioning_y);
			enemy.getAppearance();
			enemy_array.push(enemy);
		}
	}
}

function showEnemies(){
	if(enemy_array.length<=0) return true;


	let doToggle = false
	for (let i = enemy_array.length - 1; i >= 0; i--){
		enemy_array[i].show();
		if((!toggle_enemies_direction && enemy_array[i].x >= width - enemy_array[i].width)
			 || (toggle_enemies_direction && enemy_array[i].x <= 0)){
			doToggle = true; 
		}
		enemy_array[i].move(toggle_enemies_direction);
	}
	checkEnemyScreenCollision();

	if(doToggle){
		toggleEnemiesDirection();
	}
}

function checkEnemyScreenCollision(){
	for (var i = enemy_array.length - 1; i >= 0; i--) {
		if(enemy_array[i].y > height){
			enemy_array.splice(i,1);
			break;
		}
	}
}

function toggleEnemiesDirection(){
	toggle_enemies_direction = !toggle_enemies_direction;
	for (var i = enemy_array.length - 1; i >= 0; i--) {
		enemy_array[i].y += enemy_space_y;
	}
}



////////////////////// COLLISIONS //////////////////////

function bulletCollision(){

	for (let i = enemy_array.length - 1; i >= 0; i--) {
		for(let j = bullet_array.length -1; j >=0; j--){
			if(bullet_array[j].x <= enemy_array[i].x + enemy_array[i].width &&
			   bullet_array[j].x >= enemy_array[i].x &&
			   bullet_array[j].y >= enemy_array[i].y &&
			   bullet_array[j].y <= enemy_array[i].y + enemy_array[i].height){
				enemy_array.splice(i,1);
				bullet_array.splice(j,1);
				changeSpeed(true);
				addScore();
				break;
			}
		}
	}
}

function playerCollision(){
	for (var i = enemy_array.length - 1; i >= 0; i--) {
		if (enemy_array[i].x <= player.x + player.width &&
			enemy_array[i].x >= player.x - player.width &&
			enemy_array[i].y >= player.y &&
			enemy_array[i].y <= player.y + player.height){
			if(player.loseLife()){
				endGame();
				break;
			}
			else{
				enemy_array.splice(i,1);
				getSomeRest();
				break;
			}
		}
	}
}

////////////////////// BULLETS //////////////////////

function showBullets(){
	if(bullet_array.length<=0) return;
	bulletCollision();
	for (let i = bullet_array.length - 1; i >= 0; i--) {
		bullet_array[i].show();
		bullet_array[i].move();
		if(checkBulletScreenCollision(bullet_array[i])){
			bullet_array.splice(i,1);
		}
	}
}

function checkBulletScreenCollision(pBullet){
	return pBullet.y < 0;
}

function createBullet(){
	if(bullet_array.length >= player.ammunition) return;
	let bullet = new Bullet(player.x,player.y,player.bulletSpeed);
		bullet_array.push(bullet);
}



////////////////////// BONUSES //////////////////////
function Bonus(x,effect){
	this.width = 30;
	this.height = 30;
	this.x = x;
	this.y = this.height/2;

	this.show = function(){
		switch(effect){
			case "life":
				fill(255,0,0);
				break;
			case "ammo":
				fill(80,80,80);
				break;
			case "bulletSpeed":
				fill(0,255,0);
				break;
			case "speed":
				fill(0,0,255);
				break;
			default:
				fill(255);
				break;
		}

		ellipse(this.x, this.y, this.width, this.height);
	}

	this.move = function(){
		this.y += 5;
	}

	this.giveEffect = function(){
		switch(effect){
			case "life":
				player.life += 1;
				winHeart();
				break;
			case "ammo":
				player.ammunition += 1;
				break;
			case "bulletSpeed":
				player.bulletSpeed += 2;
				break;
			case "speed":
				player.speed += 2;
				break;
			default:
				fill(255);
				break;
		}
	}
}

function checkBonusScreenCollision(pBonus){
	return pBonus.y > height+(pBonus.height/2);
}

function checkBonusCollision(pBonus){
	if (pBonus.x <= player.x + player.width &&
		pBonus.x >= player.x - player.width &&
		pBonus.y >= player.y &&
		pBonus.y <= player.y + player.height){
		pBonus.giveEffect();
		return true;
	}
}

function createBonuses(){
	let bonus_number = 3;
	let effects = ["life","ammo","bulletSpeed","speed"];
	let bonus_this_time = [];

	for (let i = 0; i < bonus_number; i++){
		let random_number = Math.floor(Math.random()*effects.length)
		let random_effect = effects[random_number];
		effects.splice(random_number,1);
		bonus_this_time.push(random_effect);
	}

	for (let i = 0; i < bonus_this_time.length; i++) {
		let my_bonus = new Bonus((width/bonus_this_time.length/2) + i*(width/bonus_this_time.length),bonus_this_time[i]);
		bonus_array.push(my_bonus);
	}
}

function moveBonuses(){
	for (var i = bonus_array.length - 1; i >= 0; i--) {
		bonus_array[i].show();
		bonus_array[i].move();
		if(checkBonusCollision(bonus_array[i])){
			bonus_array.splice(i,1);
			break;
		}
		if(checkBonusScreenCollision(bonus_array[i])){
			bonus_array.splice(i,1);
			break;
		}
	}
}



////////////////////// LEVEL //////////////////////

function createLevel(){
	changing_level = false;
	createEnemies();
}

function upDifficulty(){
	if (enemies_per_line < 10){
		enemies_per_line++;
		scoreWave += 1;
	}
	if (enemies_per_line == 5 || enemies_per_line == 10){
		enemy_lines++;
		scoreWave += 2;
	}

}

function changeSpeed(pDefault){
	for (var i = enemy_array.length - 1; i >= 0; i--){
		pDefault ? enemy_array[i].speed += 0.1 : enemy_array[i].speed = 1;
	}
}

function levelFinished(){
	setTimeout(createBonuses, 1000);
	setTimeout(createLevel, 6000);
	upDifficulty();
}

function endGame(){
	destroyEverything();
	endingScreen();
	pause = true;
}

function getSomeRest(){
	for (var i = enemy_array.length - 1; i >= 0; i--) {
		enemy_array[i].y -= 3*enemy_space_y;
	}
}



function destroyEverything(){

	for (var i = enemy_array.length - 1; i >= 0; i--) enemy_array.splice(i,1);
	for (var i = bullet_array.length - 1; i >= 0; i--) bullet_array.splice(i,1);
	for (var i = heart_array.length - 1; i >= 0; i--) heart_array.splice(i,1);
	player = null;
}


////////////////////// HUD //////////////////////

function loseHeart(){
	player.life--;
	heart_array.pop();
	interval = setInterval(player.shine,200);
	y_heart -= 20;
}

function winHeart(){
	let heart = new Heart(x_heart,y_heart);
		heart_array.push(heart);
		y_heart += 20;
}

function createLife(){
	for (let i = player.life - 1; i >= 0; i--) {
		winHeart();
	}
}

function showLife(){
	for (var i = heart_array.length - 1; i >= 0; i--) heart_array[i].show();
}

function showScore(){
	fill(255);
	textSize(50);
	textFont('Georgia');
	text(str(score), width-width/10, height/10);
}

function addScore(){
	score += scoreWave;
}

function endingScreen(){
	background(1);
	fill(255);
	textAlign(CENTER);
	textSize(80);
	textFont('Georgia');
	text("GAME OVER", width/2, height/2);
	textSize(50);
	textFont('Helvetica');
	text('Score : ', width/2.1, height/2 + height/5);
	textAlign(LEFT);
	text(str(score), width/2.1 + width/10, height/2 + height/5);
}

////////////////////// DRAW //////////////////////

function draw() {
	if(pause) return;
	background(1);

	
	showLife();
	showScore();
	showBullets();
	if(showEnemies() && changing_level == false){
		changing_level = true;
		levelFinished();	
	}

	player.show();
	player.move();
	playerCollision();


	if(changing_level){
		moveBonuses();
	}
}

////////////////////// KEYS //////////////////////

function keyPressed(){
	if(keyCode == ENTER) pause = !pause;
	if(keyCode == RIGHT_ARROW) player.right = true;
	if(keyCode == LEFT_ARROW) player.left = true;
	if(keyCode == SPACE) createBullet();
}

function keyReleased(){
	if(keyCode == RIGHT_ARROW) player.right = false;
	if(keyCode == LEFT_ARROW) player.left = false;
}